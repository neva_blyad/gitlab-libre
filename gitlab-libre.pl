#!/usr/bin/perl

=pod

=encoding UTF-8

=head1 NAME

gitlab-libre
gitlab-libre.pl

=head1 VERSION

0.03

=head1 DESCRIPTION

Liberated avatars for your GitLab projects.

=head1 USAGE

./gitlab-libre.pl <char> [[[[[bg] font] colour] letter_y] output]

Example:

./gitlab-libre.pl G template1.png lobster.ttf #AFFF00 -44 G-bw.png

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

package main;

use strict;
use warnings;

use autodie 'chdir';

use FindBin;
use Image::Magick;
use Try::Tiny;

use lib "$FindBin::Bin/lib/";

use constant OUT         => 'avatar.png';
use constant BG          => 'template.png';
use constant FONT        => 'template.ttf';
use constant FONT_SIZE   => 188;
#use constant FONT_COLOUR => '#005F00'; # DarkGreen    22
use constant FONT_COLOUR => '#DF00AF'; # Magenta3    163
#use constant FONT_COLOUR => '#FF00AF'; # DeepPink1   199
#use constant FONT_COLOUR => '#AFFF00'; # GreenYellow 154
#use constant FONT_COLOUR => '#5F00DF'; # Purple3      56
use constant LETTER_Y    => -44;

our ($char, $bg, $font, $colour, $letter_y, $out) = (undef, undef, undef, undef, undef, undef);

################################################################################
# Main
################################################################################

sub usage_show
{
    print "Usage: $0 <char> [[[[[bg] font] colour] letter_y] out]\n";
    print "Example: $0 G template1.png lobster.ttf #AFFF00 -44 G-bw.png\n";
}

# Option parsing
try
{
    # Change directory path
    chdir $FindBin::Bin;

    # Option count checking
    die unless @ARGV >= 1 &&
               @ARGV <= 6;

    # Get first parameter: character
    $char = shift @ARGV;
    die if length $char != 1;

    # Get second parameter: background
    $bg       = @ARGV <= 5 ? shift @ARGV :
                             BG;
    die if -d $bg;

    # Get third parameter: font
    $font     = @ARGV <= 4 ? shift @ARGV :
                             FONT;
    die if -d $font;

    # Get fourth parameter: colour
    $colour   = @ARGV <= 3 ? shift @ARGV :
                             FONT_COLOUR;

    # Get fifth parameter: vertical positioning
    $letter_y = @ARGV <= 2 ? shift @ARGV :
                             LETTER_Y;

    # Get sixth parameter: output
    $out      = @ARGV == 1 ? shift @ARGV :
                             OUT;
}
catch
{
    usage_show();
    exit 1;
};

# Generate GitLab liberated avatar
try
{
    my $img;
    my $err;

           $img = new Image::Magick();
    $err = $img->Read('filename' => $bg) and die $err;
    $err = $img->Annotate('text'      => $char,
                          'font'      => $font,
                          'pointsize' => FONT_SIZE,
                          'fill'      => $colour,
                          'gravity'   => 'Center',
                          'y'         => $letter_y) and die $err;
    $err = $img->Write('filename' => $out) and die $err;
}
catch
{
    die "[0]: $_";
};
