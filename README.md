# NAME

gitlab-libre

# VERSION

0.03

# DESCRIPTION

Liberated avatars for your GitLab projects.

# SCREENSHOTS

![Alt Text](screenshot.png "gitlab-libre screenshot")

# USAGE

    ./gitlab-libre.pl <char> [[[[[bg] font] colour] letter_y] output]

Example:

    ./gitlab-libre.pl G template1.png lobster.ttf #AFFF00 -44 G-bw.png

letter_y is vertical positioning. It is 0 usually. Or try something different.

# COPYRIGHT AND LICENSE

    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                            <neva_blyad@lovecri.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# AUTHORS

    НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                    <neva_blyad@lovecri.es>
    Invisible Light
